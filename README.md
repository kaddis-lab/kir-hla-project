## KIR-HLA Imputation and Interaction Analysis

This is the analysis of KIR-HLA combinations in T1D using the nPOD cohort. HLA haplotype segregation is well-known in T1D, and KIR haplotype segregation has been studied as well, though to a lesser extent. Potential interactions between KIR and HLA have been even more under-explored.

Most of the nPOD cohort has been HLA-I typed to 4-digit resolution. KIR haplotype can be imputed from nPOD genotyping data using KIR*IMPUTE[^1], described in `reference/KIR_IMPUTE.pdf`. KIR haplotypes determine which KIR receptors are present, while HLA haplotypes determine the expressed HLA ligands. Interactions can be generalized as inhibitory or stimulatory (for immune cells); it is predicted that stimulatory interactions are over-represented and inhibitory interactions are under-represented in T1D.

## Contact
Author and Maintainer: Anh Nguyet Vu <avu@coh.org>


## Notes
### Data used in workflow
- nPOD (contact us)
- `1000GP_Phase3` -- go [here](https://mathgen.stats.ox.ac.uk/impute/1000GP_Phase3.html)


### KIR*Impute step
KIR imputation first requires phasing data using [SHAPEIT](https://mathgen.stats.ox.ac.uk/genetics_software/shapeit/shapeit.html)[^2]. The bash scripts for phasing are in `phase_scripts`. Note: It's actually only necessary to phase chr19, but the script `2_phase_bash` will phase all data to make it available for another project.

Submit outputs to the [imputation server](http://imp.science.unimelb.edu.au/kir/documentation), which provides `imputation_results`.

### Analysis

The folder `analysis` contains R scripts and interaction annotations used to do T1D vs AAb+ vs control comparisons. 

## References

[^1]: Vukcevic D, Traherne JA, Næss S, Ellinghaus E, Kamatani Y, Dilthey A, Lathrop M, Karlsen TH, Franke A, Moffatt M, Cookson W, Trowsdale J, McVean G, Sawcer S, Leslie S (2015). Imputation of KIR Types from SNP Variation Data. American Journal of Human Genetics 97(4):593–607.

[^2]: J. O'Connell, D. Gurdasani, O. Delaneau, et al. (2014) A general approach for haplotype phasing across the full spectrum of relatedness. PLoS Genetics. 

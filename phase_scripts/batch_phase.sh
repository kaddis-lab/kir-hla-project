#!/bin/bash

for chr in $(seq 1 22); 
do
./shapeit -B nPOD_chr/nPOD.chr$chr \
-M 1000GP_Phase3/genetic_map_chr${chr}_combined_b37.txt \
--input-1000GP_Phase3 1000GP_Phase3/1000GP_Phase3_chr$chr.hap.gz 1000GP_Phase3/1000GP_Phase3_chr$chr.legend.gz 1000GP_Phase3/1000GP_Phase3.sample \
--exclude-snp nPOD.chr$chr.align.snp.strand.exclude \
--thread 8 \
-O phased/nPOD.chr$chr.phased
done

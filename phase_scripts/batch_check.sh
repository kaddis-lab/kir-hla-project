#!/bin/bash

for chr in $(seq 1 22); 
do 
./shapeit -check \
-B nPOD_chr/nPOD.chr$chr \
-M 1000GP_Phase3/genetic_map_chr${chr}_combined_b37.txt \
--input-1000GP_Phase3 1000GP_Phase3/1000GP_Phase3_chr$chr.hap.gz 1000GP_Phase3/1000GP_Phase3_chr$chr.legend.gz 1000GP_Phase3/1000GP_Phase3.sample \
--output-log nPOD.chr$chr.align
done




